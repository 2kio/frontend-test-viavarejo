import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TemplateMasterComponent } from './template-master.component';
import { RouterTestingModule } from '@angular/router/testing';
import { NavMenuComponent } from 'src/app/components/nav-menu/nav-menu.component';

describe('TemplateMasterComponent', () => {
  let component: TemplateMasterComponent;
  let fixture: ComponentFixture<TemplateMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      declarations: [
        TemplateMasterComponent,
        NavMenuComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
