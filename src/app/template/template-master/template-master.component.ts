import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NavMenuComponent } from '../../components/nav-menu/nav-menu.component';

@Component({
	selector: 'app-template-master',
	templateUrl: './template-master.component.html',
	styleUrls: ['./template-master.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class TemplateMasterComponent implements OnInit {

	mobileMenuOpened: boolean = false;

	constructor() { }

	toggleMenu() {
		this.mobileMenuOpened = !this.mobileMenuOpened;
	}

	ngOnInit() {
	}

}
