import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { TemplateMasterComponent } from './template/template-master/template-master.component';
import { ResumoComponent } from './pages/resumo/resumo.component';
import { ConfiguracoesComponent } from './pages/configuracoes/configuracoes.component';

const routes: Routes = [
	{
		path: '',
		component: TemplateMasterComponent,
		children: [
			{ path: '', redirectTo: 'dashboard', pathMatch: 'full' },
			{ path: 'dashboard', component: DashboardComponent },
			{ path: 'resumo', component: ResumoComponent },
			{ path: 'configuracoes', component: ConfiguracoesComponent }
		]
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})

export class AppRoutingModule { }
