import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

	type: string = 'buy';
	product: string;
	price: number;
	transactions:any = (localStorage.getItem('via-transactions') == null) ? [] : JSON.parse(localStorage.getItem('via-transactions'));
	typeIcon:any[string] = {
		buy: "+",
		sell: "-"
	}
	total:number;
	
	constructor(private title: Title) {
		this.calcTotal();
		this.title.setTitle('Dashboard');
	}

	addTransaction() { // Adiciona transação e salva no localstorage
		let item = {
			type: this.type,
			product: this.product,
			price: this.price
		}
		this.transactions.unshift(item);
		localStorage.setItem('via-transactions', JSON.stringify(this.transactions));
		this.type = 'buy';
		this.product = null;
		this.price = null;
		this.calcTotal();
	}

	calcTotal() { // Calcula o total das transações
		let total = 0;
		this.transactions.forEach(el => {
			total = (el.type == 'buy') ? total+=el.price : total-=el.price;
		});
		this.total = total;
	}

	ngOnInit() {
	}

}
