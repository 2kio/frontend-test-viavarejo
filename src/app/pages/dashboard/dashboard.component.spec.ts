import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { FormsModule } from '@angular/forms';
import { CurrencyMaskModule } from 'ng2-currency-mask';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        CurrencyMaskModule
      ],
      declarations: [
        DashboardComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return number on calc total', () => {
    const fixture = TestBed.createComponent(DashboardComponent);
    const app = fixture.debugElement.componentInstance;
    app.transactions = [
      {type:"buy", product:"lorem", price:100},
      {type:"buy", product:"lorem", price:100},
      {type:"sell", product:"lorem", price:100}
    ]
    app.calcTotal();
    expect(app.total).toEqual(100);
  })

  it('should return negative calc total', () => {
    const fixture = TestBed.createComponent(DashboardComponent);
    const app = fixture.debugElement.componentInstance;
    app.transactions = [
      {type:"buy", product:"lorem", price:300},
      {type:"sell", product:"lorem", price:200},
      {type:"sell", product:"lorem", price:150}
    ]
    app.calcTotal();
    expect(app.total).toBeLessThan(0);
  })

  it('should create transaction local storage', () => {
    const fixture = TestBed.createComponent(DashboardComponent);
    const app = fixture.debugElement.componentInstance;
    app.transactions = [];
    app.type = 'buy';
    app.product = 'lorem ipsum';
    app.price = 100.50;
    app.addTransaction();
    expect(app.transactions.length).toEqual(1);
  })

  it('should increment transaction local storage', () => {
    const fixture = TestBed.createComponent(DashboardComponent);
    const app = fixture.debugElement.componentInstance;
    app.transactions = [
      {type:"buy", product:"lorem", price:100},
      {type:"buy", product:"lorem", price:100},
      {type:"sell", product:"lorem", price:100}
    ]
    app.type = 'buy';
    app.product = 'lorem ipsum';
    app.price = 100.50;
    app.addTransaction();
    expect(app.transactions.length).toEqual(4);
  })

});
